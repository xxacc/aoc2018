package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
)

var (
	inputFile = flag.String("file", "./day1/input.txt", "")
)

func main() {
	f, err := os.Open(*inputFile)
	if err != nil {
		panic(err)
	}
	s := bufio.NewScanner(f)
	var (
		freq       int64
		numIters   int
		doubleFreq int64
		freqSet    = make(map[int64]struct{})
		stop       bool
	)
	for !stop {
		f.Seek(0, 0)
		s = bufio.NewScanner(f)
		for s.Scan() && !stop {
			fmt.Println(freq)
			if _, ok := freqSet[freq]; ok {
				doubleFreq = freq
				stop = true
			} else {
				freqSet[freq] = struct{}{}
			}

			i, err := strconv.ParseInt(s.Text(), 10, 64)
			if err != nil {
				panic(err)
			}
			freq += i
		}
		numIters++
		fmt.Println("Frequency after", numIters, "rotation:", freq)
	}
	fmt.Println("First double frequency found:", doubleFreq)
	fmt.Println("Total rotations:", numIters)
}
