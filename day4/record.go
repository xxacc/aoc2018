package main

import (
	"errors"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
)

type action string

const (
	begins action = "begins"
	asleep action = "asleep"
	awake  action = "awake"
)

type record struct {
	timeStamp time.Time
	action    action
	guardID   int64
}

func newRecordFromString(s string) (r *record, err error) {
	r = &record{}
	subs := strings.Split(s, " ")
	r.timeStamp, err = time.Parse("[2006-1-2 15:04]", strings.Join(subs[0:2], " "))
	if err != nil {
		return r, err
	}
	actionIndex := 2
	if subs[2] == "Guard" {
		actionIndex = 4
		r.guardID, err = strconv.ParseInt(subs[3][1:], 10, 64)
		if err != nil {
			return r, err
		}
	}
	switch a := strings.Join(subs[actionIndex:], " "); a {
	case "begins shift":
		r.action = begins
	case "wakes up":
		r.action = awake
	case "falls asleep":
		r.action = asleep
	default:
		panic("Unknown action: " + a + " Line: " + s)
	}
	return r, nil
}

type records []*record

func (r records) Len() int {
	return len(r)
}

func (r records) Less(i int, j int) bool {
	return r[i].timeStamp.Before(r[j].timeStamp)

}

func (r records) Swap(i int, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r records) Sort() {
	sort.Sort(r)
	var lastID int64
	for _, rec := range r {
		if rec.guardID != 0 {
			lastID = rec.guardID
		} else {
			rec.guardID = lastID
		}
	}
}

func (r records) Validate() error {
	if r[0].guardID == 0 {
	}
	for i := 1; i < len(r); i++ {
		if !r[i-1].timeStamp.Before(r[i].timeStamp) {
			return errors.New("Records are not sorted")
		}
		if r[i].guardID == 0 {
		}
		if r[i].action == "" {
		}
	}
	return nil
}

type guard struct {
	id       int64
	records  records
	durSlept time.Duration
}

func newGuard(id int64) *guard {
	return &guard{id, records{}, time.Duration(0)}
}

func (g *guard) addRecord(r *record) {
	g.records = append(g.records, r)
}

func (g *guard) iterAwakeAsleep(fn func(asleep bool, start, end time.Time)) {
	var (
		isAsleep  bool
		startTime = g.records[0].timeStamp
	)

	for i, rec := range g.records {
		switch rec.action {
		case awake:
			if isAsleep {
				fn(false, startTime, g.records[i].timeStamp)
				startTime = rec.timeStamp
			}
			isAsleep = false
		case asleep:
			if !isAsleep {
				fn(true, startTime, g.records[i].timeStamp)
				startTime = rec.timeStamp
			}
			isAsleep = true
		}
	}
}

func (g *guard) findOptimalMinute() (min int, amount int) {
	asleepByMinute := make(map[int]int)
	g.iterAwakeAsleep(func(asleep bool, start, end time.Time) {
		if !asleep {
			for i := start.Minute(); i < end.Minute(); i++ {
				asleepByMinute[i]++
			}
		}
	})
	sleepyMinute := 0
	mostSlept := 0
	for minute, amountSlept := range asleepByMinute {
		if amountSlept > mostSlept {
			sleepyMinute = minute
			mostSlept = amountSlept
		}
	}
	return sleepyMinute, mostSlept
}

type guardRecords map[int64]*guard

func newGuards(r records) guardRecords {
	g := make(guardRecords)
	for _, rec := range r {
		if g[rec.guardID] == nil {
			g[rec.guardID] = newGuard(rec.guardID)
		}
		g[rec.guardID].addRecord(rec)
	}
	return g
}

func (gr guardRecords) crunchSleepTime() map[int64]time.Duration {
	m := make(map[int64]time.Duration)
	for id, g := range gr {
		mins := time.Duration(0)
		g.iterAwakeAsleep(func(asleep bool, start, end time.Time) {
			if !asleep {
				mins += end.Sub(start)
			}
		})
		m[id] = mins
	}
	return m
}

func (gr guardRecords) mostSleepy() *guard {
	m := gr.crunchSleepTime()
	spew.Dump(m)

	var (
		lastDurSlept time.Duration
		sleepiest    *guard
	)
	for id, durSlept := range m {
		if durSlept > lastDurSlept {
			sleepiest = &guard{
				id:       id,
				records:  gr[id].records,
				durSlept: durSlept,
			}
			lastDurSlept = durSlept
		}
	}
	return sleepiest
}
