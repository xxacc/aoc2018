package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

var (
	inputFile = flag.String("file", "./input.txt", "")
)

func main() {
	flag.Parse()

	f, err := os.Open(*inputFile)
	if err != nil {
		panic(err)
	}
	s := bufio.NewScanner(f)

	var recs records
	for s.Scan() {
		rec, err := newRecordFromString(s.Text())
		if err != nil {
			panic(err)
		}
		recs = append(recs, rec)
	}
	recs.Sort()
	if err := recs.Validate(); err != nil {
		panic(err)
	}
	guards := newGuards(recs)
	sleepiest := guards.mostSleepy()
	optimalMinute, _ := sleepiest.findOptimalMinute()
	fmt.Printf("Most sleepy: %v, optimal minute: %v, ans: %v\n",
		sleepiest.id, optimalMinute, sleepiest.id*int64(optimalMinute))

	for _, g := range guards {
		min, amount := g.findOptimalMinute()
		fmt.Println(g.id, min, amount)
	}
}
