package main

import (
	"bytes"
	"container/list"
	"flag"
	"fmt"
	"io/ioutil"
	"unicode"
)

var (
	inputFile = flag.String("file", "./input.txt", "")
)

func main() {
	flag.Parse()

	data, err := ioutil.ReadFile(*inputFile)
	if err != nil {
		panic(err)
	}
	data = bytes.TrimSpace(data)
	fmt.Println("Original:\t" + string(data))

	change := true
	i := 0
	var bad rune
	for bad = 0x61; bad < 0x7B; bad++ {
		l := list.New()
		for _, b := range data {
			l.PushBack(b)
		}
		for change {
			change = react(l, bad)
			i++
			// fmt.Println("Round", i, "complete")
		}
		if react(l, bad) {
			panic("There are still doubles")
		}
		var result []byte
		for e := l.Front(); e != nil; e = e.Next() {
			result = append(result, e.Value.(byte))
		}
		fmt.Println("Result:\t\t", bad, len(result))
	}
}

func react(l *list.List, bad rune) (changed bool) {
	var last *list.Element
	for e := l.Front(); e != nil; e = e.Next() {
		if bad == unicode.ToLower(rune(e.Value.(byte))) {
			// fmt.Println("Remove:\t\t", string(e.Value.(byte)))
			l.Remove(e)
			return true
		}
		if e.Prev() == nil {
			last = e
			continue
		}
		if last.Value.(byte) == e.Value.(byte)+0x20 || last.Value.(byte) == e.Value.(byte)-0x20 {
			// fmt.Println("Remove:\t\t", string(last.Value.(byte)), string(e.Value.(byte)))
			l.Remove(last)
			l.Remove(e)
			return true
		}
		last = e
	}
	return false
}
