package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

var (
	inputFile = flag.String("file", "./input.txt", "")
)

func main() {
	flag.Parse()

	f, err := os.Open(*inputFile)
	if err != nil {
		panic(err)
	}
	s := bufio.NewScanner(f)

	var ()

	fab := newFabric()
	for s.Scan() {
		str := s.Text()
		c, err := newClaimFromString(str)
		if err != nil {
			panic(err)
		}
		fab.Add(c)
	}
	fmt.Println("Overlap:", fab.countOverlap())
	fmt.Printf("Unique: %+v\n", fab.findNonOverlappingClaim())
}
