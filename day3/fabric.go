package main

import "fmt"

type vector struct {
	X, Y int
}

type cell struct {
	claims []claim
}

type fabric struct {
	coords map[vector][]claim
	claims map[claim][]vector
}

func newFabric() *fabric {
	return &fabric{make(map[vector][]claim), make(map[claim][]vector)}
}

func (f fabric) Add(c claim) {
	for x := c.startX; x < c.startX+c.width; x++ {
		for y := c.startY; y < c.startY+c.height; y++ {
			f.coords[vector{x, y}] = append(f.coords[vector{x, y}], c)
			f.claims[c] = append(f.claims[c], vector{x, y})
		}
	}
}

func (f fabric) countOverlap() (i int) {
	for _, v := range f.coords {
		if len(v) >= 2 {
			i++
		}
	}
	return i
}

func (f fabric) findNonOverlappingClaim() claim {
	for c, vectors := range f.claims {
		uniq := true
		for _, vec := range vectors {
			if len(f.coords[vec]) > 1 {
				uniq = false
				break
			}
		}
		if uniq {
			return c
		}
	}
	return claim{}
}

func printFabric(fab [][]int) {
	for _, row := range fab {
		for _, cell := range row {
			fmt.Print(cell, " ")
		}
		fmt.Println("")
	}

}
