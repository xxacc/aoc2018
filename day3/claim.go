package main

import (
	"strconv"
	"strings"
)

type claim struct {
	id     int
	startX int
	startY int
	width  int
	height int
}

// #1 @ 342,645: 25x20

func newClaimFromString(s string) (c claim, err error) {
	subs := strings.Split(s, " ")
	c.id, err = findID(subs)
	if err != nil {
		return c, err
	}
	c.startX, err = findStartX(subs)
	if err != nil {
		return c, err
	}
	c.startY, err = findStartY(subs)
	if err != nil {
		return c, err
	}
	c.width, err = findWidth(subs)
	if err != nil {
		return c, err
	}
	c.height, err = findHeight(subs)
	if err != nil {
		return c, err
	}
	return c, nil
}

func findID(subs []string) (int, error) {
	id, err := strconv.ParseInt(subs[0][1:], 10, 64)
	return int(id), err
}

func findStartX(subs []string) (int, error) {
	coords := strings.Split(subs[2], ",")
	id, err := strconv.ParseInt(coords[0], 10, 64)
	return int(id), err
}

func findStartY(subs []string) (int, error) {
	coords := strings.Split(subs[2], ",")
	id, err := strconv.ParseInt(coords[1][:len(coords[1])-1], 10, 64)
	return int(id), err
}

func findWidth(subs []string) (int, error) {
	size := strings.Split(subs[3], "x")
	id, err := strconv.ParseInt(size[0], 10, 64)
	return int(id), err
}
func findHeight(subs []string) (int, error) {
	size := strings.Split(subs[3], "x")
	id, err := strconv.ParseInt(size[1], 10, 64)
	return int(id), err
}
