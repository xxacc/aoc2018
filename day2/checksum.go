package main

import "fmt"

type checksum struct {
	count2 int
	count3 int
}

func (cs *checksum) Add(s string) {
	contains2, contains3 := getCount(s)
	// printIter(s, contains2, contains3)
	if contains2 {
		cs.count2++
	}
	if contains3 {
		cs.count3++
	}
}

func (cs *checksum) Total() int {
	return cs.count2 * cs.count3
}

func getCount(s string) (contains2, contains3 bool) {
	runeCount := make(map[rune]int)
	for _, r := range s {
		runeCount[r]++
	}
	for _, v := range runeCount {
		if v == 2 {
			contains2 = true
		} else if v == 3 {
			contains3 = true
		}
	}
	return contains2, contains3
}

func printIter(s string, contains2, contains3 bool) {
	fmt.Print(s, " len=", len(s), ": ")
	if contains2 {
		fmt.Print("Contains 2, ")
	}
	if contains3 {
		fmt.Print("Contains 3")
	}
	fmt.Println("")
}
