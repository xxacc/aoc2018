package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

var (
	inputFile = flag.String("file", "./input.txt", "")
)

func main() {
	flag.Parse()

	f, err := os.Open(*inputFile)
	if err != nil {
		panic(err)
	}
	s := bufio.NewScanner(f)

	cs := &checksum{}
	df := &diff{}

	for s.Scan() {
		str := s.Text()
		cs.Add(str)
		df.Add(str)
	}

	fmt.Println("Checksum", cs.Total())

	id1, id2 := df.Crunch()
	fmt.Println("ID 1:", id1, "ID2:", id2)
	id := removeDiff(id1, id2)
	fmt.Println("Common letters:", id, "len:", len(id))
}
