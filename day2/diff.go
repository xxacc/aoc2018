package main

import (
	"strings"
)

type diff struct {
	strings []string
}

func (d *diff) Add(s string) {
	d.strings = append(d.strings, s)
}

func (d *diff) Crunch() (string, string) {
	for _, s := range d.strings {
		for _, o := range d.strings {
			if s == o {
				continue
			}
			if len(removeDiff(s, o)) == 25 {
				return s, o
			}
		}
	}
	return "", ""
}

func removeDiff(s, o string) string {
	var res strings.Builder
	for i := 0; i < len(s); i++ {
		if s[i] == o[i] {
			res.WriteByte(s[i])
		}
	}
	return res.String()
}

func divergeString(s, o string) string {
	if len(s) != len(o) {
		panic("Strings must be the same length")
	}

	var (
		divergeFound bool
	)
	for i := 0; i < len(s); i++ {
		if s[i] != o[i] {
			if !divergeFound {
				divergeFound = true
			} else {
				return s[:i]
			}
		}
	}
	return s
}
